# GIDY

GIDY (GuIDance sYstems) is package that provides some guidance laws in different programming languages.

## List of implemented laws

- Proportional Navigation Guidance (PNG)
- Augumented Proportional Navigation Guidance (APNG)
- Beam-Rider (BM)
- Beam-Rider with Lead-Lag Compensator (BMLC)
- Command to LOS (CLOS)

## Environments

- Fortran
- Matlab
- Julia
- Python

Scripts are not equivalent among the different languages. More details are present in the corresponding folder.

![](images/lospic.png)
