program tacticalEngagement

  implicit none

  real, parameter :: PI = 3.1415926
  real, parameter :: Deg180 = 180.0
  real :: sigma, sigma_dot
  real :: R, R_dot, Vc
  integer :: N = 3
  real :: dt = 0.0001
  real :: t = 0.0, t_limit = 50.0
  character (len=1) :: strategy = "P"
  character(len=1) :: ans

  type System
    real, dimension(2) :: pos, vel, acc
    real :: command ! acceleration command
    real :: he ! heading error
    real :: gamma
  end type System

  type(System) :: pursuer, mark !target

  ! intialize pursuer
  pursuer%pos = (/0.0,0.0/)
  pursuer%he = deg2rad(40.0)
  pursuer%gamma = 0.0 + pursuer%he
  pursuer%vel = (/1000*cos(pursuer%gamma),1000*sin(pursuer%gamma)/)
  pursuer%acc = (/0.0,0.0/)
  pursuer%command = 0.0

  ! initialize target
  mark%pos = (/40000.0,10000.0/)
  mark%gamma = deg2rad(150.0)
  mark%vel = (/700*cos(mark%gamma),700*sin(mark%gamma)/)
  mark%acc = (/0.0,0.0/)
  mark%command = 0.0

  print *,"Do you want to setup a custom scenario? (y/n)"
  read *, ans
  if (ans == "y") then
    print *, "Set navigation ratio N."
    read *, N
    print *, "Set strategy. (P/A)"
    read *, strategy
    print *, "Set target acceleration. [g]"
    read *, mark%command
    mark%command = mark%command * 9.81
  else
    print *,"Using default settings."
    print "(a5,i1)", "N = ", N
    print "(a11,a1)", "Strategy: ", strategy
    print "(a23,f3.1,a1)", "Target acceleration = ", mark%command, "g"
  end if
  print *, ""

  sigma = atan((mark%pos(2)-pursuer%pos(2))/(mark%pos(1)-pursuer%pos(1)))
  R = norm2(mark%pos - pursuer%pos)
  sigma_dot = ((mark%vel(2)-pursuer%vel(2))*(mark%pos(1)-pursuer%pos(1)) - (mark%vel(1)-pursuer%vel(1))*(mark%pos(2)-pursuer%pos(2)))/(R**2)
  Vc = -((mark%vel(1)-pursuer%vel(1))*(mark%pos(1)-pursuer%pos(1)) - (mark%vel(2)-pursuer%vel(2))*(mark%pos(2)-pursuer%pos(2)))/R


  do while (Vc > 0)

    sigma = atan((mark%pos(2)-pursuer%pos(2))/(mark%pos(1)-pursuer%pos(1)))
    R = norm2(mark%pos - pursuer%pos)
    sigma_dot = ((mark%vel(2)-pursuer%vel(2))*(mark%pos(1)-pursuer%pos(1)) - (mark%vel(1)-pursuer%vel(1))*(mark%pos(2)-pursuer%pos(2)))/(R**2)
    Vc = -((mark%vel(1)-pursuer%vel(1))*(mark%pos(1)-pursuer%pos(1)) - (mark%vel(2)-pursuer%vel(2))*(mark%pos(2)-pursuer%pos(2)))/R

    ! compute command
    ! pursuer%command = N * Vc * sigma_dot / (pursuer%gamma - sigma)
    call compute_command(N, strategy, pursuer%gamma, sigma, sigma_dot, Vc, R, mark%command, pursuer%command)

    pursuer%acc = (/-pursuer%command*sin(pursuer%gamma), pursuer%command*cos(pursuer%gamma)/)
    pursuer%vel = pursuer%vel + pursuer%acc*dt
    pursuer%pos = pursuer%pos + pursuer%vel*dt

    mark%acc = (/-mark%command*sin(mark%gamma), mark%command*cos(mark%gamma)/)
    mark%vel = mark%vel + mark%acc*dt
    mark%pos = mark%pos + mark%vel*dt

    t = t + dt
    if (t >= t_limit) then
      print *, "No interception occured..."!, t
      EXIT
    end if

  end do

  if (Vc < 0) then
    print 100, t, R
    100 format("Interception occured at time t = ",f5.2,"s with range distance R = ", f5.2,"m.")
  end if



contains
  real function deg2rad(deg) result(rad)
    implicit none
    real, intent(in) :: deg
    rad = deg * PI / Deg180
  end function deg2rad

  subroutine compute_command(N, strategy, pursuer_gamma, sigma, sigma_dot, Vc, R, At, acc_command)
    ! implicit none
    real, intent(in) :: sigma, sigma_dot, Vc, R, At, pursuer_gamma
    integer, intent(in) :: N
    character, intent(in) :: strategy
    real :: N2
    real, intent(out) :: acc_command

    select case(strategy)
    case ("P")
      acc_command = N * Vc * sigma_dot / cos(pursuer_gamma - sigma)
    case ("A")
      N2 = N / cos(pursuer_gamma - sigma)
      acc_command = N2 * Vc * sigma_dot + 0.5 * N2 * At
    case default

    end select


  end subroutine compute_command

end program tacticalEngagement
