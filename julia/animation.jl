# tactical engagement animation

################################################################################

function animatedEngagement(scen)

    space_ = 2000;
    min_x = min(minimum(scenario.P[1,1:scenario.c_]),minimum(scenario.T[1,1:scenario.c_])) - space_;
    max_x = max(maximum(scenario.P[1,1:scenario.c_]),maximum(scenario.T[1,1:scenario.c_])) + space_;
    min_y = min(minimum(scenario.P[2,1:scenario.c_]),minimum(scenario.T[2,1:scenario.c_])) - space_;
    max_y = max(maximum(scenario.P[2,1:scenario.c_]),maximum(scenario.T[2,1:scenario.c_])) + space_;

    s_ = Int(round(scenario.c_ / 100));


    for t=1:s_:scenario.c_

        plt = plot([scen.P[1,t],scen.T[1,t]],[scen.P[2,t],scen.T[2,t]], linestyle=:dash, color=:black, xlims=(min_x,max_x), ylims=(min_y,max_y));
        scatter!([scen.T[1,t]], [scen.T[2,t]], color=:blue, markersize=8, legend=:false);
        scatter!([scen.P[1,t]], [scen.P[2,t]], color=:red, markersize=8);

        σ_ = @sprintf "%.2f" rad2deg(scen.Σ[1,t])
        𝑅_ = @sprintf "%.2f" scen.R[1,t]

        plot!(1,annotation=(min_x+1000,max_y-100,Plots.text(L"\sigma : ", 10, :black, :center)));
        annotate!([(min_x+1000,max_y-700,Plots.text(L"R :",10,:black,:centetr))]);
        annotate!([(min_x+1800,max_y-60,Plots.text(string(σ_, " deg"), 7, :black, :left))]);
        annotate!([(min_x+1800,max_y-740,Plots.text(string(𝑅_, "m"), 7, :black, :left))]);

        xlabel!(L"x \hspace{0.1cm} (m)");
        ylabel!(L"y \hspace{0.1cm} (m)");
        display(plt)
    end

    # last step

    t = scenario.c_

    plt = plot([scen.P[1,t],scen.T[1,t]],[scen.P[2,t],scen.T[2,t]], linestyle=:dash, color=:black, xlims=(min_x,max_x), ylims=(min_y,max_y));
    scatter!([scen.T[1,t]], [scen.T[2,t]], color=:blue, markersize=8, legend=:false);
    scatter!([scen.P[1,t]], [scen.P[2,t]], color=:red, markersize=8);

    σ_ = @sprintf "%.2f" rad2deg(scen.Σ[1,t])
    𝑅_ = @sprintf "%.2f" scen.R[1,t]

    plot!(1,annotation=(min_x+1000,max_y-100,Plots.text(L"\sigma : ", 10, :black, :center)));
    annotate!([(min_x+1000,max_y-700,Plots.text(L"R :",10,:black,:centetr))]);
    annotate!([(min_x+1800,max_y-60,Plots.text(string(σ_, " deg"), 7, :black, :left))]);
    annotate!([(min_x+1800,max_y-740,Plots.text(string(𝑅_, "m"), 7, :black, :left))]);

    xlabel!(L"x \hspace{0.1cm} (m)");
    ylabel!(L"y \hspace{0.1cm} (m)");
    display(plt)


end

################################################################################

# run a simulation
scenario = tacticalEngagement(;Λ=4, he=deg2rad(40.0));

# video
animatedEngagement(scenario)
