# compare guidance laws for different value of naviagtion ratio Λ

################################################################################

function compareCommandSignals(scen3,scen4,scen5)

    t3 = collect(1:1:scen3.c_)*scen3.Δt
    t4 = collect(1:1:scen4.c_)*scen3.Δt
    t5 = collect(1:1:scen5.c_)*scen3.Δt

    e_ = 500

    s3 = Int(round(scen3.c_/e_))
    s4 = Int(round(scen4.c_/e_))
    s5 = Int(round(scen5.c_/e_))

    plt=plot(t3[1:s3:end], scen3.A[1,1:s3:scen3.c_]./9.81, label=L"\Lambda = 3",legend=:topleft);
    plot!(t4[1:s4:end], scen4.A[1,1:s4:scen4.c_]./9.81, label=L"\Lambda = 4";)
    plot!(t5[1:s5:end], scen5.A[1,1:s5:scen5.c_]./9.81, label=L"\Lambda = 5");
    xlabel!(L"t \hspace{0.1cm} (s)");
    ylabel!(L"A \hspace{0.1cm} (g)");
    display(plt)

end

################################################################################

function compareRange(scen3,scen4,scen5)

    t3 = collect(1:1:scen3.c_)*scen3.Δt
    t4 = collect(1:1:scen4.c_)*scen3.Δt
    t5 = collect(1:1:scen5.c_)*scen3.Δt

    e_ = 500

    s3 = Int(round(scen3.c_/e_))
    s4 = Int(round(scen4.c_/e_))
    s5 = Int(round(scen5.c_/e_))

    plt=plot(t3[1:s3:end], scen3.R[1,1:s3:scen3.c_]./scen3.R[1,1], label=L"\Lambda = 3",legend=:topleft);
    plot!(t4[1:s4:end], scen4.R[1,1:s4:scen4.c_]./scen4.R[1,1], label=L"\Lambda = 4";)
    plot!(t5[1:s5:end], scen5.R[1,1:s5:scen5.c_]./scen5.R[1,1], label=L"\Lambda = 5");
    xlabel!(L"t \hspace{0.1cm} (s)");
    ylabel!(L"\left(\frac{R}{R_0}\right)^{(\Lambda-2)}");
    display(plt)



end

################################################################################


scen3 = tacticalEngagement(;Λ=3, he=deg2rad(40.0));
scen4 = tacticalEngagement(;Λ=4, he=deg2rad(40.0));
scen5 = tacticalEngagement(;Λ=5, he=deg2rad(40.0));

compareCommandSignals(scen3,scen4,scen5)

compareRange(scen3,scen4,scen5)







# using InteractBulma, DataStructures   # future work
# d = OrderedDict()
# d["plot1"] = plot(rand(10))
# d["plot2"] = scatter(rand(10))
# t = tabulator(d)
