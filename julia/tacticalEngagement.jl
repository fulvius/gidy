using StaticArrays
using LinearAlgebra
using LaTeXStrings
using Printf
using Plots
gr()

################################################################################

mutable struct System
    p::MArray{Tuple{2,1},Float64,2,2}
    γ::Float64
    v::MArray{Tuple{2,1},Float64,2,2}
    a::MArray{Tuple{2,1},Float64,2,2}
    ac::Float64
end

################################################################################

mutable struct Scenario
    Δt::Float64
    P::Array{Float64,2} # pursuer pos
    A::Array{Float64,2} # pursuer command
    T::Array{Float64,2} # target pos
    R::Array{Float64,2} # range store
    Σ::Array{Float64,2} # los angle store
    c_::Int64
end

################################################################################

function computeCommand(strategy, Λ, γ𝑝, σ, σ̇ , 𝑉𝑐, At)

    if (strategy == "P")

        return Λ * 𝑉𝑐 * σ̇ / cos(γ𝑝 - σ)

    elseif (strategy == "A")

        N = Λ / cos(γ𝑝 - σ)
        return N * 𝑉𝑐 * σ̇  + 0.5 * N * At

        # return N * 𝑉𝑐 * σ̇  + 0.5 * N * At/9.81
    end

end

################################################################################

function tacticalEngagement(;Λ=3, strategy="P", he=deg2rad(40), At=0.0, Δt = 0.0001)

    t = 0.0
    tf = 100.0

    vp = 1000
    pursuer = System([0.0,0.0],he,[vp*cos(he),vp*sin(he)],[0.0,0.0],0.0)

    vt = 400
    γt = deg2rad(170)
    target = System([30000.0,10000.0],γt,[vt*cos(γt),vt*sin(γt)],[0.0,0.0],At*9.81)

    l = Int(tf/Δt)
    scenario = Scenario(Δt,zeros(2,l),zeros(1,l),zeros(2,l),zeros(1,l),zeros(1,l),0)

    σ = atan((target.p[2]-pursuer.p[2])/(target.p[1]-pursuer.p[1]))
    𝑅 = norm(target.p - pursuer.p,2)
    σ̇ = ((target.v[2]-pursuer.v[2])*(target.p[1]-pursuer.p[1]) - (target.v[1]-pursuer.v[1])*(target.p[2]-pursuer.p[2]))/(𝑅^2)
    𝑉𝑐 = -((target.v[1]-pursuer.v[1])*(target.p[1]-pursuer.p[1]) + (target.v[2]-pursuer.v[2])*(target.p[2]-pursuer.p[2]))/𝑅

    while (𝑉𝑐 > 0)

        # σ = atan((target.p[2]-pursuer.p[2])/(target.p[1]-pursuer.p[1]))
        σ = atan((target.p[2]-pursuer.p[2]),(target.p[1]-pursuer.p[1]))

        𝑅 = norm(target.p - pursuer.p)
        σ̇ = ((target.v[2]-pursuer.v[2])*(target.p[1]-pursuer.p[1]) - (target.v[1]-pursuer.v[1])*(target.p[2]-pursuer.p[2]))/(𝑅^2)
        𝑉𝑐 = -((target.v[1]-pursuer.v[1])*(target.p[1]-pursuer.p[1]) + (target.v[2]-pursuer.v[2])*(target.p[2]-pursuer.p[2]))/𝑅

        Ac = computeCommand(strategy, Λ, pursuer.γ, σ, σ̇ , 𝑉𝑐, target.ac)
        pursuer.ac = Ac
        pursuer.a = [-pursuer.ac*sin(pursuer.γ), pursuer.ac*cos(pursuer.γ)]
        pursuer.v = pursuer.v + pursuer.a * Δt
        pursuer.p = pursuer.p + pursuer.v * Δt
        pursuer.γ = atan(pursuer.v[2]/pursuer.v[1])

        target.a = [-target.ac*sin(target.γ), target.ac*cos(target.γ)]
        target.v = target.v + target.a * Δt
        target.p = target.p + target.v * Δt
        target.γ = atan(target.v[2]/target.v[1])

        t += Δt

        if (t >= tf)
            println("No interception occured...",t)
            println(𝑅)
            break
        end

        # store
        scenario.c_ += 1
        scenario.P[:,scenario.c_] = pursuer.p
        scenario.T[:,scenario.c_] = target.p
        scenario.A[1,scenario.c_] = pursuer.ac
        scenario.R[1,scenario.c_] = 𝑅
        scenario.Σ[1,scenario.c_] = σ

    end

    if (𝑉𝑐 < 0)
        time_ = @sprintf "%.2f" t
        distance_ = @sprintf "%.2f" 𝑅
        println("Interception occured at time t = $(time_)s with range distance R = $(distance_)m.")
    end

    return  scenario

end

################################################################################
