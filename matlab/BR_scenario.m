% get data from simulink

br_eng.T = reshape([BR.Data(1,1,:), BR.Data(1,2,:)],2,[])'; 
br_eng.P = reshape([BR.Data(1,3,:), BR.Data(1,4,:)],2,[])'; 

% animated scenario
animatedBeamRiderEngagement(br_eng)

