%% animatedBeamRiderEngagement

function animatedBeamRiderEngagement(eng_data)

    close all

    s_ = 2000;
    min_x = min(min(eng_data.P(:,1)),min(eng_data.T(:,1))) - s_;
    max_x = max(max(eng_data.P(:,1)),max(eng_data.T(:,1))) + s_;
    min_y = min(min(eng_data.P(:,2)),min(eng_data.T(:,2))) - s_;
    max_y = max(max(eng_data.P(:,2)),max(eng_data.T(:,2))) + s_;
    
    l = size(eng_data.P,1);
    step_ = round(l/100);
    
    figure(1)
    set(gcf,'Position',[50,50,600,460])
    for t=1:step_:l
        clf(gcf)
        hold on
        
        plot([0,eng_data.T(t,1)],[0,eng_data.T(t,2)],'k--','LineWidth',1)
        [angle,radius] = cart2pol(eng_data.T(t,1),eng_data.T(t,2));
        radius = radius + 500;
        angle_dev = deg2rad(2);
        tplus = [radius*cos(angle+angle_dev),radius*sin(angle+angle_dev)];
        tminus = [radius*cos(angle-angle_dev),radius*sin(angle-angle_dev)];
        patch([0 tplus(1) tminus(1) 0],[0 tplus(2) tminus(2) 0],[255 206 0]/255)
        plot([0,tplus(1)],[0,tplus(2)],'-','color',[0.8500 0.3250 0.0980])
        plot([0,tminus(1)],[0,tminus(2)],'-','color',[0.8500 0.3250 0.0980])
        plot([0,eng_data.T(t,1)],[0,eng_data.T(t,2)],'k--','LineWidth',1)
       
        
%         tplus = [linspace(0,radius*cos(angle+angle_dev),100); linspace(0,radius*sin(angle+angle_dev),100)];
%         tminus = [linspace(0,radius*cos(angle-angle_dev),100); linspace(0,radius*sin(angle-angle_dev),100)];
%         plot(tplus(1,:),tplus(2,:),'-','color',[0.8500 0.3250 0.0980])
%         plot(tminus(1,:),tminus(2,:),'-','color',[0.8500 0.3250 0.0980])
%         plot([0,tplus(1)],[0,tplus(2)],'-','color',[0.8500 0.3250 0.0980])
%         plot([0,tminus(1)],[0,tminus(2)],'-','color',[0.8500 0.3250 0.0980])
%         patch([x fliplr(x)], [y1 fliplr(y2)], 'g')
            
        
        
        
        %txt = strcat('$\sigma = ',sprintf('%.2f',rad2deg(eng_data.sigma(t))),' deg$');
        %text(min_x+ s_/3 ,max_y-s_/3,txt,'FontSize',12,'Interpreter','Latex')
        %txt2 = strcat('$\dot{\sigma} = ',sprintf('%.3f',rad2deg(sigma_dot)),' deg/s$');
        %text(min_x+200, max_y-1200,txt2,'FontSize',10,'Interpreter','Latex')
        
        plot(eng_data.T(t,1),eng_data.T(t,2),'bo','MarkerSize',8,'MarkerFaceColor','b')
        plot(eng_data.P(t,1),eng_data.P(t,2),'ro','MarkerSize',8,'MarkerFaceColor','r')
        
        plot(eng_data.T(1:t-1,1),eng_data.T(1:t-1,2),'b-','LineWidth',1)
        plot(eng_data.P(1:t-1,1),eng_data.P(1:t-1,2),'r-','LineWidth',1)
        
        %plot([eng_data.P(t,1),eng_data.T(t,1)],[eng_data.P(t,2),eng_data.T(t,2)],'k--','LineWidth',1)
        grid on
        %legend('Target','Pursuer')
        xlim([min_x max_x])
        ylim([min_y max_y])
        xlabel('$x$ [m]','Interpreter','Latex')
        ylabel('$y$ [m]','Interpreter','Latex')
        drawnow
        
    end


end