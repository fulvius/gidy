%% animatedEngagement

function animatedEngagement(eng_data)

    close all

    s_ = 2000;
    min_x = min(min(eng_data.P(:,1)),min(eng_data.T(:,1))) - s_;
    max_x = max(max(eng_data.P(:,1)),max(eng_data.T(:,1))) + s_;
    min_y = min(min(eng_data.P(:,2)),min(eng_data.T(:,2))) - s_;
    max_y = max(max(eng_data.P(:,2)),max(eng_data.T(:,2))) + s_;
    
    l = size(eng_data.P,1);
    step_ = round(l/100);
    
    figure(1)
    set(gcf,'Position',[50,50,600,460])
    for t=1:step_:l
        clf(gcf)
        hold on
        
        txt = strcat('$\sigma = ',sprintf('%.2f',rad2deg(eng_data.sigma(t))),' deg$');
        text(min_x+ s_/3 ,max_y-s_/3,txt,'FontSize',12,'Interpreter','Latex')
        %txt2 = strcat('$\dot{\sigma} = ',sprintf('%.3f',rad2deg(sigma_dot)),' deg/s$');
        %text(min_x+200, max_y-1200,txt2,'FontSize',10,'Interpreter','Latex')
        
        plot(eng_data.T(t,1),eng_data.T(t,2),'bo','MarkerSize',8,'MarkerFaceColor','b')
        plot(eng_data.P(t,1),eng_data.P(t,2),'ro','MarkerSize',8,'MarkerFaceColor','r')
        
        plot(eng_data.T(1:t-1,1),eng_data.T(1:t-1,2),'b-','LineWidth',1)
        plot(eng_data.P(1:t-1,1),eng_data.P(1:t-1,2),'r-','LineWidth',1)
        
        plot([eng_data.P(t,1),eng_data.T(t,1)],[eng_data.P(t,2),eng_data.T(t,2)],'k--','LineWidth',1)
        grid on
        %legend('Target','Pursuer')
        xlim([min_x max_x])
        ylim([min_y max_y])
        xlabel('$x$ [m]','Interpreter','Latex')
        ylabel('$y$ [m]','Interpreter','Latex')
        drawnow
        
    end


end