close all

% compare PNG for different values of navigation-ration N

eng_data3 = tacticalEngagement(3);
eng_data4 = tacticalEngagement(4);
eng_data5 = tacticalEngagement(5);
eng_data6 = tacticalEngagement(6);

eng_data3.command = ms22g(eng_data3.command);
eng_data4.command = ms22g(eng_data4.command);
eng_data5.command = ms22g(eng_data5.command);
eng_data6.command = ms22g(eng_data6.command);

t3 = (0:1:(length(eng_data3.command)-1))*eng_data3.dt;
t4 = (0:1:(length(eng_data4.command)-1))*eng_data4.dt;
t5 = (0:1:(length(eng_data5.command)-1))*eng_data5.dt;
t6 = (0:1:(length(eng_data6.command)-1))*eng_data6.dt;

figure(1)
set(gcf,'Position',[50,50,600,460])
hold on
plot(t3,eng_data3.command)
plot(t4,eng_data4.command)
plot(t5,eng_data5.command)
plot(t6,eng_data6.command)
grid on
xlabel('$t$ [s]','Interpreter','Latex')
ylabel('$A_p$ [g]','Interpreter','Latex','FontSize',14)
legend("$\Lambda_n=3$","$\Lambda_n=4$","$\Lambda_n=5$","$\Lambda_n=6$",'Location','best','Interpreter','Latex','FontSize',12)

figure(2)
set(gcf,'Position',[700,50,600,460])
hold on
plot(t3,(eng_data3.range/eng_data3.range(1)).^(3-2))
plot(t4,(eng_data4.range/eng_data4.range(1)).^(4-2))
plot(t5,(eng_data5.range/eng_data5.range(1)).^(5-2))
plot(t6,(eng_data6.range/eng_data6.range(1)).^(6-2))
grid on
xlabel('$t$ [s]','Interpreter','Latex')
ylabel('$\left(\frac{R}{R_0}\right)^{\Lambda_n - 2}$','Interpreter','Latex','FontSize',18)
legend("$\Lambda_n=3$","$\Lambda_n=4$","$\Lambda_n=5$","$\Lambda_n=6$",'Location','best','Interpreter','Latex','FontSize',12)
