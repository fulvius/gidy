%% ms22g

function acc_g = ms22g(acc_ms2)
    % comvert from m/s^2 to g
    acc_g = acc_ms2 / 9.81;
end