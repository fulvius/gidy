%% plotScenario

% plot acceleration command and range distance for a given scenario

function plotScenario(eng_data)

    close all
    
    eng_data.command = ms22g(eng_data.command);
    t = (0:1:(length(eng_data.command)-1))*eng_data.dt;

    figure(1)
    set(gcf,'Position',[50,50,600,460])
    hold on
    plot(t,eng_data.command)
    grid on
    xlabel('$t$ [s]','Interpreter','Latex')
    ylabel('$A_p$ [g]','Interpreter','Latex','FontSize',14)

    figure(2)
    set(gcf,'Position',[700,50,600,460])
    hold on
    plot(t,(eng_data.range/eng_data.range(1)).^(eng_data.Lambda_n-2))
    grid on
    xlabel('$t$ [s]','Interpreter','Latex')
    ylabel('$\left(\frac{R}{R_0}\right)^{\Lambda_n - 2}$','Interpreter','Latex','FontSize',18)

end