%% tacticalEngagement

function [eng_data] = tacticalEngagement(Lambda_n, he, At, strategy)
    

    % Lambda_n = navigation ratio
    % he = heading error [deg]
    % At = Target acceleration [g]
    % strategy = kind of PNG
    
    switch(nargin)
        case 0
            scenario.Lambda_n = 3;
            he = 0;
            At = 0;
            strategy = 'P';
        case 1
            scenario.Lambda_n = Lambda_n;
            he = 0;
            At = 0;
            strategy = 'P';
        case 2
            scenario.Lambda_n = Lambda_n;
            At = 0;
            strategy = 'P';
        case 3
            scenario.Lambda_n = Lambda_n;
            strategy = 'P';
        case 4
            scenario.Lambda_n = Lambda_n;
    end
    
    eng_data.P = [];
    eng_data.T = [];
    eng_data.command = [];
    eng_data.sigma = [];
    eng_data.sigma_dot = [];
    eng_data.range = [];
    eng_data.Lambda_n = scenario.Lambda_n;

    % pursuer
    P.p = [0, 0];
    P.gamma = deg2rad(he);
    P.v_ = 900;
    P.v = [P.v_* cos(P.gamma), P.v_*sin(P.gamma)];
    P.a = [0, 0];
    P.Ap = 0;

    % target
    T.p = [40000, 10000];
    T.v_ = 300;
    T.At = At * 9.81;
    T.a = [0,0];
    T.gamma = deg2rad(150);
    T.v = [T.v_* cos(T.gamma), T.v_*sin(T.gamma)];

    scenario.sigma = atan((T.p(2) - P.p(2))/(T.p(1) - P.p(1)));
    scenario.R = norm(T.p - P.p);
    scenario.sigma_dot = ((T.v(2)-P.v(2))*(T.p(1)-P.p(1)) - (T.v(1)-P.v(1))*(T.p(2)-P.p(2)))/(scenario.R^2);
    scenario.Vc = - ((T.v(1)-P.v(1))*(T.p(1)-P.p(1)) - (T.v(2)-P.v(2))*(T.p(2)-P.p(2)))/scenario.R; 

    scenario.dt = 0.004;
    eng_data.dt = scenario.dt;

    %counter = 0;


    while (scenario.Vc>0)

%         if (scenario.R < 1000)
%             dt = 0.0001;
%             scenario.dt = 0.01;
%         else
%             scenario.dt = 0.01;
%         end

        eng_data.P = [eng_data.P; P.p];
        eng_data.T = [eng_data.T; T.p];

        % compute command
        scenario.sigma = atan((T.p(2) - P.p(2))/(T.p(1) - P.p(1)));
        scenario.R = norm(T.p - P.p);
        scenario.sigma_dot = ((T.v(2)-P.v(2))*(T.p(1)-P.p(1)) - (T.v(1)-P.v(1))*(T.p(2)-P.p(2)))/(scenario.R^2);
        scenario.Vc = - ((T.v(1)-P.v(1))*(T.p(1)-P.p(1)) + (T.v(2)-P.v(2))*(T.p(2)-P.p(2)))/scenario.R;
        eng_data.sigma = [eng_data.sigma; scenario.sigma];
        eng_data.sigma_dot = [eng_data.sigma_dot; scenario.sigma_dot];
        eng_data.range = [eng_data.range; scenario.R];
        
        
        Ap = PursuerCommand(strategy, scenario, P, T);
        eng_data.command = [eng_data.command; Ap];
        P.Ap = Ap;
        P.a = [-P.Ap*sin(P.gamma), P.Ap*cos(P.gamma)];
        P.v = P.v + P.a * scenario.dt;
        P.p = P.p + P.v * scenario.dt;
        P.gamma = atan(P.v(2)/P.v(1));
        
        
        T.a = [-At*sin(T.gamma), At*cos(T.gamma)];
        T.v = T.v + T.a*scenario.dt;
        T.p = T.p + T.v * scenario.dt;
        T.gamma = atan(T.v(2)/T.v(1));

        %counter = counter + 1;


    end

end

function Ap = PursuerCommand(strategy, scenario, P, T)

    switch strategy
        
        case 'P' % Proportional Navigation Guidance

            Ap = scenario.Lambda_n * scenario.Vc * scenario.sigma_dot / cos(P.gamma - scenario.sigma);
            
        case 'A' % Augmented Proportional Navigation Guidance
            
            N_ = scenario.Lambda_n / cos(P.gamma - scenario.sigma);
            Ap = N_ * scenario.Vc * scenario.sigma_dot + 0.5 * N_ * (T.At);
            
        otherwise

            error(sprintf('Not available strategy! Use of of the following:\n\nP: PNG\nA: APNG'));
    end
            
        


end
    