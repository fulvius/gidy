import numpy as np
import pygame

def game(mis_pos, target_pos):

    pygame.init()
    display_width = 800
    display_height = 600

    gameDisplay = pygame.display.set_mode((display_width,display_height))
    pygame.display.set_caption('ProNav')

    black = (0,0,0)
    white = (255,255,255)
    blue = (27,164,255)

    ang_off = 180

    # scale to window size
    xmin = np.fmin(mis_pos[:,0].min(), target_pos[:,0].min()) - 10000.0
    xmax = np.fmax(mis_pos[:,0].max(), target_pos[:,0].max()) + 10000.0
    ymin = np.fmin(mis_pos[:,1].min(), target_pos[:,1].min()) - 10000.0
    ymax = np.fmax(mis_pos[:,1].max(), target_pos[:,1].max()) + 10000.0

    mis_pos[:,0] = (mis_pos[:,0] - xmin)/(xmax - xmin)*(display_width)
    mis_pos[:,1] = (mis_pos[:,1] - ymin)/(ymax - ymin)*(display_height)
    target_pos[:,0] = (target_pos[:,0] - xmin)/(xmax - xmin)*(display_width)
    target_pos[:,1] = (target_pos[:,1] - ymin)/(ymax - ymin)*(display_height)


    clock = pygame.time.Clock()

    missile_img = pygame.image.load('images/mis.png')
    target_img = pygame.image.load('images/air.png')
    sky_img = pygame.image.load('images/sky.jpg')

    boom_img = pygame.image.load('images/boom.jpg')

    missile_img = pygame.transform.scale(missile_img, (10,30))
    target_img = pygame.transform.scale(target_img, (60,140))
    sky_img = pygame.transform.scale(sky_img, (display_width,display_height))
    boom_img = pygame.transform.scale(boom_img, (display_width,display_height))


    def missile(x,y,angle):
        im_tmp = pygame.transform.rotate(missile_img, np.rad2deg(angle)+ang_off)
        gameDisplay.blit(im_tmp,(x,y))
        # pygame.display.update()

    def target(x,y,angle):
        im_tmp = pygame.transform.rotate(target_img, np.rad2deg(angle)+ang_off)
        gameDisplay.blit(im_tmp,(x,y))

    for i in range(mis_pos.shape[0]):

        gameDisplay.fill(blue)
        # gameDisplay.blit(sky_img, (0,0))

        missile(mis_pos[i,0],mis_pos[i,1],mis_pos[i,2])
        target(target_pos[i,0],target_pos[i,1],target_pos[i,2])

        pygame.display.update()
        clock.tick(50)

    gameDisplay.blit(boom_img, (0,0))
    # pygame.display.update()
    pygame.display.flip()
    clock.tick(20)

    pygame.quit()
    quit()
