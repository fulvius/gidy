import numpy as np

class ProNav(object):

    def __init__(self):

        # missile
        self.p_m = np.zeros((2,1))
        self.v_m = np.zeros((2,1))
        self.a_m = np.zeros((2,1))

        self.gamma_m = np.pi/8

        self.p_m_old = np.zeros((2,1))
        self.v_m_old = np.zeros((2,1))


        # target
        self.p_t = np.zeros((2,1))
        self.v_t = np.zeros((2,1))
        self.a_t = np.zeros((2,1))

        self.gamma_t = 3*np.pi/4

        self.p_t_old = np.zeros((2,1))
        self.v_t_old = np.zeros((2,1))


        self.los = 0.0
        self.los_old = 0.0

        self.los_angle = 0.0
        self.los_angle_old = 0.0


        self.dt = 0.1

    def set_pm(self, x, z, he):

        ''' Set missile position '''

        self.p_m = np.array([x,z]).reshape(2,1)
        self.p_m_old = self.p_m
        # self.gamma_m = ang_off
        self.he = he    # heading error

    def set_vm(self, vm):

        ''' Set missile velocity '''

        self.vm = vm
        self.lead_angle =  np.arcsin(self.vt * np.sin(self.los_angle + self.gamma_t) / self.vm)     # leading angle
        self.thet = self.los_angle + self.lead_angle

        self.v_m = np.array([self.vm*np.cos(self.thet + self.he), self.vm*np.sin(self.thet + self.he)]).reshape(2,1)
        self.v_m_old = self.v_m

    def set_pt(self, x, z, gamma_t=np.pi/6):

        ''' Set target position '''

        self.p_t = np.array([x,z]).reshape(2,1)
        self.p_t_old = self.p_t
        self.gamma_t = gamma_t

    def set_vt(self, vt):

        ''' Set target velocity '''

        self.vt = vt
        self.v_t = np.array([-self.vt*np.cos(self.gamma_t),self.vt*np.sin(self.gamma_t)]).reshape(2,1)
        self.v_t_old = self.v_t

    def set_at(self, acc, target_acc=False):

        ''' Set target acceleration '''

        self.target_acc = target_acc

        # if (not self.target_acc):
        #     self.a_t = np.zeros((2,1))
        # else:
        #     print('Accelerating target!')
        # print(acc)
        self.a_t = acc.reshape(2,1)

        # print(self.a_t)

    def update_missile(self):

        ''' Update missile velocity and position '''


        vt = np.linalg.norm(self.v_m)
        self.lead_angle =  np.arcsin(vt * np.sin(self.los_angle + self.gamma_t) / self.vm)     # leading angle
        self.thet = self.los_angle + self.lead_angle

        self.v_m_old = np.array([self.vm*np.cos(self.thet + self.he), self.vm*np.sin(self.thet + self.he)]).reshape(2,1)


        self.v_m = self.a_m * self.dt + self.v_m_old


        self.p_m += self.v_m * self.dt
        # self.p_m[0,0] *= np.cos(self.gamma_m)
        # self.p_m[1,0] *= np.sin(self.gamma_m)



        self.v_m_old = self.v_m
        # self.p_m_old = self.p_m

        self.gamma_m = np.arctan(self.v_m[1,0]/self.v_m[0,0])


    def update_target(self):

        ''' Update target velocity and position '''

        # if (self.target_acc):

        self.a_t[0,0] = -np.sin(self.gamma_t) * self.a_t[0,0]
        self.a_t[1,0] = np.cos(self.gamma_t) * self.a_t[1,0]

        self.v_t = self.a_t * self.dt + self.v_t_old
        self.p_t = self.v_t * self.dt + self.p_t_old
        # self.p_t += self.v_t * self.dt

        self.v_t_old = self.v_t
        self.p_t_old = self.p_t

        self.gamma_t = np.arctan(self.v_t[1,0]/self.v_t[0,0])

    def compute_los(self):

        ''' Compute los_range and los_angle '''

        self.los = np.linalg.norm(self.p_t - self.p_m)      # los == R
        self.los_angle = np.arctan((self.p_t[1,0]-self.p_m[1,0]) / (self.p_t[0,0]-self.p_m[0,0]))   # sigma

    def compute_los_dot(self):

        ''' Compute derivative of los_range and los_angle '''

        self.los_dot = ((self.v_t[0,0]-self.v_m[0,0])*(self.p_t[0,0]-self.p_m[0,0]) + (self.v_t[1,0]-self.v_m[1,0])*(self.p_t[1,0]-self.p_m[1,0])) / self.los
        self.los_angle_dot = ((self.v_t[1,0]-self.v_m[1,0])*(self.p_t[0,0]-self.p_m[0,0]) - (self.v_t[0,0]-self.v_m[0,0])*(self.p_t[1,0]-self.p_m[1,0])) / (self.los*self.los)

    def command_PNG(self, ln=3.5):

        self.type = 'Proportional Navigation Guidance'
        self.ln = ln
        self.vc = - self.los_dot    # closing velocity
        # print('Angle rate:', self.los_angle_dot)
        self.command = self.ln * self.vc * self.los_angle_dot   #/ np.cos(self.los_angle - self.gamma_m)

        self.a_m[0,0] = -np.sin(self.los_angle) * self.command
        self.a_m[1,0] = np.cos(self.los_angle) * self.command


    def command_APNG(self, ln=3.5):

        self.command_PNG()
        self.type = 'Augmented Proportional Navigation Guidance'

        # new command element
        new_gain = self.a_t * self.ln / 2
        # print('New gain',new_gain)
        self.a_m[0,0] =  -np.sin(self.los_angle) * (self.command + new_gain[0,0])
        self.a_m[1,0] =  np.cos(self.los_angle) * (self.command + new_gain[1,0])
