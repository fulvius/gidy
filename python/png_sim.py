import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import pygame
import time

from png import ProNav
from game import game

def main():

    sys = ProNav()

    # target initialization
    sys.set_pt(40000.0,1500.0,np.pi/4)
    sys.set_vt(400)
    # sys.set_at()
    sys.set_at(target_acc=True, acc=np.array([2,2]))

    # missile initialization
    sys.set_pm(0.0,0.0, np.pi/4)
    sys.set_vm(1000.0)

    sys.compute_los()
    sys.update_target()
    sys.update_missile()

    m_ = [np.vstack((sys.p_m, sys.gamma_m))]
    t_ = [np.vstack((sys.p_t, sys.gamma_t))]
    a_ = list()
    los_ = list()
    sim_time = 0.0
    tm_ = list()
    i = 0
    sim_length = 4000

    # print('Before to start, a_t is', sys.a_t.T)
    # time.sleep(2)
    while(sys.los > 10):

        sim_time += sys.dt
        tm_.append(sim_time)
        i += 1

        los_.append(sys.los)

        if (sys.los < 10):
            sys.dt = 0.01
            # print('changed sampling time')

        sys.compute_los()
        sys.compute_los_dot()
        sys.command_PNG(ln=3.5)
        # sys.command_APNG(ln=3.5)

        # sys.compute_lin_los()
        # sys.command_lin()

        sys.update_missile()

        sys.update_target()

        m_.append(np.vstack((sys.p_m, sys.gamma_m)))
        t_.append(np.vstack((sys.p_t, sys.gamma_t)))
        a_.append(sys.a_m)
        # print(sys.a_m)

        # print('In loop, a_t is', sys.a_t.T)
        # time.sleep(1)

        if (sys.los < 10):
            print('---------------------------------')
            # print(i)
            print('Interception at time %.2fs with los = %.3f' % (sim_time, sys.los))
            # print(sys.p_t.T)
            # print(sys.p_m.T)
            print('---------------------------------')
            break

    m = np.asarray(m_, dtype=np.float32).reshape(len(m_),3)
    t = np.asarray(t_, dtype=np.float32).reshape(len(t_),3)
    a = np.asarray(a_, dtype=np.float32).reshape(len(a_),2)
    tm = np.asarray(tm_, dtype=np.float32)
    los = np.asarray(los_, dtype=np.float32)

    # uncomment to run the game
    game(m,t)

    # plt.plot(tm, a[:,0],'b')
    # plt.plot(tm, a[:,1],'g')
    # plt.grid()
    plt.show()


#################################################

    xmin = np.fmin(m[:,0].min(), t[:,0].min()) - 1000.0
    xmax = np.fmax(m[:,0].max(), t[:,0].max()) + 1000.0
    ymin = np.fmin(m[:,1].min(), t[:,1].min()) - 1000.0
    ymax = np.fmax(m[:,1].max(), t[:,1].max()) + 1000.0

    fig = plt.figure()
    ax = plt.axes(xlim=(xmin, xmax), ylim=(ymin, ymax))

    time_text = ax.text(0.01, 0.9, '', transform=ax.transAxes)
    los_text = ax.text(0.01, 0.8, '', transform=ax.transAxes)


    line1, = ax.plot([],[], lw=2, color='b', label='missile')
    line2, = ax.plot([],[], lw=2, color='r', label='target')
    line3, = ax.plot([],[], lw=1, color='k', linestyle='dashed', label='los')

    lines = [line1, line2, line3]


    def init():

        for line in lines:
            line.set_data([],[])

        time_text.set_text('')
        los_text.set_text('')

        return tuple(lines) + (time_text,los_text,)


    def animate(i):

        time_text.set_text('Time %.2fs' % tm[i])
        los_text.set_text('Los %.0fm' % los[i])

        lines[0].set_data(m[:i,0],m[:i,1])
        lines[1].set_data(t[:i,0],t[:i,1])
        lines[2].set_data(np.linspace(m[i,0],t[i,0],10),np.linspace(m[i,1],t[i,1],10))

        return tuple(lines) + (time_text, los_text,)

    anim = animation.FuncAnimation(fig, animate, init_func=init, frames=i, interval=10, blit=True, repeat=True)

    plt.grid()
    plt.legend(loc='best')
    plt.title(sys.type + ' simulation')
    plt.xlabel('X [m]')
    plt.ylabel('Y [m]')
    plt.show()
################################################################################



if __name__ == '__main__':
    main()
